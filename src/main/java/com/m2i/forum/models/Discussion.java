package com.m2i.forum.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "discussions")
public class Discussion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 200)
    private String title;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "section_id")
    private Section section;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private User author;

    @JsonIgnore
    @OneToMany(mappedBy = "discussion", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Message> messages = new ArrayList<>();

    @NotBlank
    @Column(columnDefinition = "TEXT")
    private String firstMessage;
    
    @ManyToMany
	@JoinTable(
			  name = "discussion_like", 
			  joinColumns = @JoinColumn(name = "discussion_id"), 
			  inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> likes;

    private LocalDateTime createdAt;

    public Discussion() {
    }

    public Discussion(String title, Section section, User author, String firstMessage, Set<User> likes) {
        this.title = title;
        this.section = section;
        this.author = author;
        this.firstMessage = firstMessage;
        this.likes = likes;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id id à définir
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title title à définir
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return section
     */
    public Section getSection() {
        return section;
    }

    /**
     * @param section section à définir
     */
    public void setSection(Section section) {
        this.section = section;
    }

    /**
     * @return author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * @param author author à définir
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * @return messages
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * @return firstMessage
     */
    public String getFirstMessage() {
        return firstMessage;
    }

    /**
     * @param firstMessage author à définir
     */
    public void setFirstMessage(String firstMessage) {
        this.firstMessage = firstMessage;
    }

    /**
     * @return createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * createdAt à définir
     */
    public void setCreatedAt() {
        this.createdAt = LocalDateTime.now();
    }

	public Set<User> getLikes() {
		return likes;
	}

	public void setLikes(Set<User> likes) {
		this.likes = likes;
	}
	
	public void addLike(User like) {
		this.likes.add(like);
	}
}
