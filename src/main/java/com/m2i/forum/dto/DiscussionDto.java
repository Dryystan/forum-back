package com.m2i.forum.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import com.m2i.forum.models.Message;
import com.m2i.forum.models.Section;

public class DiscussionDto {

    public Long id;
    public String title;
    public Section section;
    public UserDto author;
    public List<Message> messages;
    public String firstMessage;
    public Set<UserDto> likes;
    public LocalDateTime createdAt;

    public DiscussionDto() {
    }

    /**
     * @param title
     * @param section
     * @param author
     * @param messages
     * @param firstMessage
     * @param likes
     * @param createdAt
     */
    public DiscussionDto(Long id, String title, Section section, UserDto author, List<Message> messages, String firstMessage, Set<UserDto> likes, LocalDateTime createdAt) {
        this.id = id;
        this.title = title;
        this.section = section;
        this.author = author;
        this.messages = messages;
        this.firstMessage = firstMessage;
        this.likes = likes;
        this.createdAt = createdAt;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id id à définir
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title title à définir
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return section
     */
    public Section getSection() {
        return section;
    }

    /**
     * @return author
     */
    public UserDto getAuthor() {
        return author;
    }

    /**
     * @param author author à définir
     */
    public void setAuthor(UserDto author) {
        this.author = author;
    }

    /**
     * @return messages
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * @return firstMessage
     */
    public String getFirstMessage() {
        return firstMessage;
    }

    /**
     * @param firstMessage firstMessage à définir
     */
    public void setFirstMessage(String firstMessage) {
        this.firstMessage = firstMessage;
    }

	public Set<UserDto> getLikes() {
		return likes;
	}

	public void setLikes(Set<UserDto> likes) {
		this.likes = likes;
	}

    /**
     * @return createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * createdAt à définir
     */
    public void setCreatedAt() {
        this.createdAt = LocalDateTime.now();
    }
}
