package com.m2i.forum.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.m2i.forum.security.services.UserDetailsServiceImpl;

@Configuration
@EnableAutoConfiguration
public class ServiceConfig {
	
	@Bean
	public UserDetailsServiceImpl userDetailsServiceFactory() {
		return new UserDetailsServiceImpl();
	}
}

