package com.m2i.forum.dao;

import java.util.Optional;

import com.m2i.forum.models.Discussion;

public interface DiscussionDao extends GenericDao<Discussion, Long> {

    public Optional<Discussion> findByTitle(String title);

    public void deleteById(Long id);
}
