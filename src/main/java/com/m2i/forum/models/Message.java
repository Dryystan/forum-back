package com.m2i.forum.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(columnDefinition = "TEXT")
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "discussion_id")
    private Discussion discussion;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public Message() {
    }

    public Message(User author, String content, Discussion discussion) {
        this.author = author;
        this.content = content;
        this.discussion = discussion;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id id à définir
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content content à définir
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return author
     */
    public User getAuthor() {
        return author;
    }

    /**
     * @param author author à définir
     */
    public void setAuthor(User author) {
        this.author = author;
    }

    /**
     * @return discussion
     */
    public Discussion getDiscussion() {
        return discussion;
    }

    /**
     * @param discussion discussion à définir
     */
    public void setDiscussion(Discussion discussion) {
        this.discussion = discussion;
    }

    /**
     * @return createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * createdAt à définir
     */
    public void setCreatedAt() {
        this.createdAt = LocalDateTime.now();
    }

    /**
     * @return updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * updatedAt à définir
     */
    public void setUpdatedAt() {
        this.updatedAt = LocalDateTime.now();
    }
}
