package com.m2i.forum.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m2i.forum.dto.DiscussionDto;
import com.m2i.forum.dto.UserDto;
import com.m2i.forum.models.Discussion;
import com.m2i.forum.models.Message;
import com.m2i.forum.models.Section;
import com.m2i.forum.models.User;
import com.m2i.forum.services.impl.DiscussionServiceImpl;
import com.m2i.forum.services.impl.UserServiceImpl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DiscussionMapper {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    DiscussionServiceImpl discussionService;

    public DiscussionDto toDto(Discussion discussion) {
        Long id = discussion.getId();
        String title = discussion.getTitle();
        Section section = discussion.getSection();
        UserDto author = userMapper.toDto(discussion.getAuthor());
        List<Message> messages = discussion.getMessages();
        String firstMessage = discussion.getFirstMessage();
        Set<UserDto> likes = discussion.getLikes().stream().map(user -> userMapper.toDto(user)).collect(Collectors.toSet());
        LocalDateTime createdAt = discussion.getCreatedAt();

        return new DiscussionDto(id, title, section, author, messages, firstMessage, likes, createdAt);
    }

    public Discussion toEntity(DiscussionDto discussionDto) {
        User user = userService.findById(discussionDto.getAuthor().getId());
        Set<User> likes = new HashSet<User>();
        if(discussionDto.getLikes() != null) {
        	likes = discussionDto.getLikes().stream().map(userDto -> userService.findById(userDto.getId())).collect(Collectors.toSet());
        }
        return new Discussion(discussionDto.getTitle(), discussionDto.getSection(), user, discussionDto.getFirstMessage(), likes);
    }
}
