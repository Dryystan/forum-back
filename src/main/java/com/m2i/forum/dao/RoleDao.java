package com.m2i.forum.dao;

import java.util.Optional;

import com.m2i.forum.enums.Roles;
import com.m2i.forum.models.Role;

public interface RoleDao extends GenericDao<Role, Long>{
	public Optional<Role> findByName(Roles name);
}
