package com.m2i.forum.enums;

public enum Roles {
	USER("USER"),
	MODERATOR("MODERATOR"),
	ADMIN("ADMIN");
	
	private final String label;
	
	Roles(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
}
