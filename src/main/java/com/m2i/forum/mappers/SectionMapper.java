package com.m2i.forum.mappers;

import org.springframework.stereotype.Component;

import com.m2i.forum.dto.SectionDto;
import com.m2i.forum.models.Discussion;
import com.m2i.forum.models.Section;

import java.util.List;

@Component
public class SectionMapper {

    public SectionDto toDto(Section section) {
        Long id = section.getId();
        String title = section.getTitle();
        String detail = section.getDetail();
        List<Discussion> discussions = section.getDiscussions();

        return new SectionDto(id, title, detail, discussions);
    }

    public Section toEntity(SectionDto sectionDto) {
        return new Section(sectionDto.getDetail(), sectionDto.getTitle());
    }

}
