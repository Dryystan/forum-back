package com.m2i.forum.dao.impl;

import org.springframework.stereotype.Repository;

import com.m2i.forum.dao.DiscussionDao;
import com.m2i.forum.models.Discussion;

import javax.persistence.Query;
import java.util.Optional;

@Repository
public class DiscussionDaoImpl extends GenericDaoImpl<Discussion, Long> implements DiscussionDao {

    @Override
    public Optional<Discussion> findByTitle(String title) {
        Query query = entityManager.createQuery("from Discussion WHERE title=:title");
        query.setParameter("title", title);
        Discussion discussion = null;
        try {
            discussion = (Discussion) query.getSingleResult();
        } catch (Exception e) {
            logger.error("Error finding discussion with title: " + title);
        }
        return Optional.ofNullable(discussion);
    }

    @Override
    public void deleteById(Long id) {
        entityManager.createQuery("DELETE FROM Discussion WHERE id=:id")
                .setParameter("id", id).executeUpdate();
    }
}
