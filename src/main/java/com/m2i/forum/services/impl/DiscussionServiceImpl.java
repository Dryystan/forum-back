package com.m2i.forum.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.m2i.forum.dao.DiscussionDao;
import com.m2i.forum.dao.GenericDao;
import com.m2i.forum.dao.MessageDao;
import com.m2i.forum.models.Discussion;
import com.m2i.forum.services.DiscussionService;

import java.util.Optional;

@Service
public class DiscussionServiceImpl extends GenericServiceImpl<Discussion, Long> implements DiscussionService {

    private DiscussionDao discussionDao;
    private MessageDao messageDao;

    @Autowired
    public DiscussionServiceImpl(@Qualifier("discussionDaoImpl") GenericDao<Discussion, Long> genericDao, DiscussionDao discussionDao, MessageDao messageDao) {
        super(genericDao);
        this.discussionDao = discussionDao;
        this.messageDao = messageDao;
    }

    @Override
    public Optional<Discussion> findByTitle(String title) {
        return discussionDao.findByTitle(title);
    }

    @Override
    public void deleteById(Long id) {
        messageDao.deleteByDiscussionId(id);
        discussionDao.deleteById(id);
    }
}
