package com.m2i.forum.dto;

import java.time.LocalDateTime;

import com.m2i.forum.models.Discussion;

public class MessageDto {

    public Long id;
    public String content;
    public UserDto author;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
    public Discussion discussion;

    public MessageDto(Long id, String content, UserDto author, Discussion discussion, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.id = id;
        this.content = content;
        this.author = author;
        this.discussion = discussion;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id id à définir
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return message
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content content à définir
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return author
     */
    public UserDto getAuthor() {
        return author;
    }

    /**
     * @param author author à définir
     */
    public void setAuthor(UserDto author) {
        this.author = author;
    }

    /**
     * @return createdAt
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * createdAt à définir
     */
    public void setCreatedAt() {
        this.createdAt = LocalDateTime.now();
    }

    /**
     * @return updatedAt
     */
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * updatedAt à définir
     */
    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return discussion
     */
    public Discussion getDiscussion() {
        return discussion;
    }

    /**
     * discussion à définir
     */
    public void setDiscussion(Discussion discussion) {
        this.discussion = discussion;
    }
}
