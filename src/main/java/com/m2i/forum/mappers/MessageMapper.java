package com.m2i.forum.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m2i.forum.dto.MessageDto;
import com.m2i.forum.dto.UserDto;
import com.m2i.forum.models.Discussion;
import com.m2i.forum.models.Message;
import com.m2i.forum.models.User;
import com.m2i.forum.services.impl.UserServiceImpl;

import java.time.LocalDateTime;

@Component
public class MessageMapper {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserServiceImpl userService;

    public MessageDto toDto(Message message) {
        Long id = message.getId();
        String content = message.getContent();
        UserDto author = userMapper.toDto(message.getAuthor());
        Discussion discussion = message.getDiscussion();
        LocalDateTime createdAt = message.getCreatedAt();
        LocalDateTime updatedAt = message.getUpdatedAt();

        return new MessageDto(id, content, author, discussion, createdAt, updatedAt);
    }

    public Message toEntity(MessageDto messageDto) {
        User user = userService.findById(messageDto.getAuthor().getId());
        return new Message(user, messageDto.getContent(), messageDto.getDiscussion());
    }
}
