package com.m2i.forum.services;

import java.util.Optional;

import com.m2i.forum.enums.Roles;
import com.m2i.forum.models.Role;

public interface RoleService extends GenericService<Role, Long>{
	public Optional<Role> findByName(Roles name);
}
