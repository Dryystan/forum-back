package com.m2i.forum.services;

import java.util.Optional;

import com.m2i.forum.models.Section;

public interface SectionService extends GenericService<Section, Long> {

    public Optional<Section> findByTitle(String title);

    public void deleteById(Long id);
}
