package com.m2i.forum.dao;

import java.util.List;
import java.util.Optional;

import com.m2i.forum.dto.MessageDto;

public interface MessageDao {

    public void deleteById(Long id);
    public void deleteByDiscussionId(Long id);
    public Optional<List<MessageDto>> findMessagesByDiscussionId(Long id);
}
