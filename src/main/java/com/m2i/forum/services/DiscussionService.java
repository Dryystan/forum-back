package com.m2i.forum.services;

import java.util.Optional;

import com.m2i.forum.models.Discussion;

public interface DiscussionService extends GenericService<Discussion, Long> {

    public Optional<Discussion> findByTitle(String title);

    public void deleteById(Long id);
}
