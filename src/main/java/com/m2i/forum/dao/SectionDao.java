package com.m2i.forum.dao;

import java.util.Optional;

import com.m2i.forum.models.Section;

public interface SectionDao extends GenericDao<Section, Long> {

    public Optional<Section> findByTitle(String title);

    public void deleteById(Long id);
}
