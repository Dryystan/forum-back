package com.m2i.forum.dao.impl;

import org.springframework.stereotype.Repository;

import com.m2i.forum.dao.MessageDao;
import com.m2i.forum.dto.MessageDto;
import com.m2i.forum.models.Message;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository
public class MessageDaoImpl extends GenericDaoImpl<Message, Long> implements MessageDao {

    @Override
    public Optional<List<MessageDto>> findMessagesByDiscussionId(Long id) {
        Query query = entityManager.createQuery(
                "SELECT m FROM Message m WHERE m.discussion.id=:id");
        query.setParameter("id", id).getResultList();
        List<MessageDto> messages = null;
        try {
            messages = query.getResultList();
        } catch (Exception e) {
            logger.error("Error finding messages for discussion id : " + id);
        }
        return Optional.ofNullable(messages);
    }

    @Override
    public void deleteById(Long id) {
        entityManager.createQuery("DELETE FROM Message WHERE id=:id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void deleteByDiscussionId(Long id) {
        entityManager.createQuery("DELETE FROM Message m WHERE m.discussion.id =:id")
                .setParameter("id", id).executeUpdate();
    }


}
