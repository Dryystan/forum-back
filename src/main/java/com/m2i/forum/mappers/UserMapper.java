package com.m2i.forum.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.m2i.forum.dto.UserDto;
import com.m2i.forum.models.Discussion;
import com.m2i.forum.models.Role;
import com.m2i.forum.models.User;
import com.m2i.forum.services.impl.UserServiceImpl;

import java.util.Set;

@Component
public class UserMapper {

    @Autowired
    UserServiceImpl userService;
    
	public UserDto toDto(User user){
		Long id = user.getId();
        String username = user.getUsername();
        String email = user.getEmail();
        Set<Role> roles = user.getRoles();

        return new UserDto(id, username, email, roles);
    }
	
	public User toUser(UserDto userDto) {
		return new User(userDto.getUsername(), userDto.getEmail());
	}
}
