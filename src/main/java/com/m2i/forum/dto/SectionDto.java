package com.m2i.forum.dto;

import java.util.List;

import com.m2i.forum.models.Discussion;

public class SectionDto {

    public Long id;
    public String title;
    public String detail;
    public List<Discussion> discussions;

    public SectionDto(Long id, String title, String detail, List<Discussion> discussions) {
        this.id = id;
        this.title = title;
        this.detail = detail;
        this.discussions = discussions;
    }

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id id à définir
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title title à définir
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * @param detail detail à définir
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * @return discussions
     */
    public List<Discussion> getDiscussions() {
        return discussions;
    }
}
